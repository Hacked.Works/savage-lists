# savage-lists

This project is an attempt to create a digital equivalent of the ToDo lists 
that Adam Savage describes in his book 'Every Tool Is A Hammer'. If you want 
to get a better understanding of the concept you should read the 
[WIRED arcticle](https://www.wired.com/story/adam-savage-lists-more-lists-power-checkboxes/).

It is also my first use of bottle as a framework.

## Documentation

Documentation is maintained in the `doc` folder and currently there is only 
some basic design documentation with a few user stories. The user stories are
also maintained as [issues](https://codeberg.org/Hacked.Works/savage-lists/issues?q=&labels=87076).

## Installing and running

This software requires [Pyhton](https://www.python.org/) 3.8 or greater as
well as [Bottle](https://bottlepy.org/) 0.12 or greater to be installed. To
run the software simply sync this repository and then navigate to the
`src/savage_list` folder where you then run `python3 main.py`. This will start
a webserver listening on `localhost:8080`. You can then access your
savage-lists via http://localhost:8080/