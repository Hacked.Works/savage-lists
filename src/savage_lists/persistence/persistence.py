import sqlite3
import uuid

from models.task import Task, TaskStatus

TASK_TABLE_DEFINITION = '''
CREATE TABLE tasks (
	id GUID,
	title TEXT,
	status INTEGER DEFAULT (0),
	parent GUID,
	CONSTRAINT TASKS_PK PRIMARY KEY (id),
	CONSTRAINT tasks_FK FOREIGN KEY (parent) REFERENCES tasks(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);
'''

sqlite3.register_adapter(uuid.UUID, lambda u: u.bytes_le)
sqlite3.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))

_db_connection = sqlite3.connect(
    '../../database/savage_tasks.db', detect_types=sqlite3.PARSE_DECLTYPES)


def _setup():
    cursor = _db_connection.cursor()
    results = cursor.execute(
        "SELECT name FROM sqlite_master WHERE name='tasks'")
    if results.fetchone() is None:
        cursor.execute(TASK_TABLE_DEFINITION)


def add_task(task: Task):
    task_id = task.task_id
    title = task.title
    status = task.status.value
    parent_id = None
    if task.parent is not None:
        parent_id = task.parent.task_id

    cursor = _db_connection.cursor()
    result = cursor.execute(
        "INSERT INTO tasks (id, title, status, parent) VALUES (?, ?, ?, ?)", (task_id, title, status, parent_id))
    _db_connection.commit()


def update_task(task: Task):
    cursor = _db_connection.cursor()
    result = cursor.execute(
        "UPDATE tasks SET title=?, status=? WHERE id=?", (task.title, task.status.value, task.task_id))
    _db_connection.commit()


def delete_task_recursively(task: Task):
    cursor = _db_connection.cursor()
    # First recursively delete all children
    for child_task in task.children:
        delete_task_recursively(child_task)
    # Then delete the task it self
    result = cursor.execute(
        "DELETE FROM tasks WHERE id=?", ([task.task_id]))
    _db_connection.commit()


def _create_task_from_record(task_record, parent):
    task_id = task_record[0]
    task_title = task_record[1]
    task_status = task_record[2]
    # parent_id = task_record[3]

    task = Task(title=task_title, task_id=task_id, parent=parent)
    task.status = TaskStatus(task_status)
    return task


def read_all_tasks():
    tasks = {}
    _read_all_tasks(tasks, None)
    return tasks.values()


def _read_all_tasks(tasks, parent: Task):
    cursor = _db_connection.cursor()

    result = None
    if parent is None:
        result = cursor.execute(
            "SELECT * FROM tasks WHERE parent IS NULL")
    else:
        parent_id = parent.task_id
        result = cursor.execute(
            "SELECT * FROM tasks WHERE parent=?", ([parent_id]))

    task_records = result.fetchall()
    for task_record in task_records:
        task = _create_task_from_record(task_record, parent)
        tasks[task.task_id] = task
        _read_all_tasks(tasks, task)


def read_task(id: uuid):
    cursor = _db_connection.cursor()
    result = cursor.execute("SELECT * FROM tasks WHERE id = ?", ([id]))
    task_records = result.fetchall()

    if len(task_records) < 1:
        return None

    task = _create_task_from_record(task_records[0], None)
    tasks = {}
    _read_all_tasks(tasks, task)
    return task


_setup()
