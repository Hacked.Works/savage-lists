from enum import Enum
import uuid


class Task:
    def __init__(self, title: str, task_id=None, parent=None) -> None:
        if task_id is None:
            self._task_id = uuid.uuid4()
        else:
            self._task_id = task_id

        self._parent = parent
        if parent is not None:
            parent._add_child(self)
        self._children = []

        self.title = title
        self.status = TaskStatus.CREATED

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, title: str) -> None:
        if title is None:
            raise ValueError('No title was provided for the task!')
        title = title.strip()
        if len(title) == 0:
            raise ValueError(
                'No title was provided for the task or it only consisted of white spaces')
        self._title = title

    @property
    def task_id(self) -> uuid:
        return self._task_id

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return self._children

    def _add_child(self, child) -> None:
        self._children.append(child)

    def _remove_child(self, child) -> None:
        self._children.remove(child)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if isinstance(status, TaskStatus):
            self._status = status
        else:
            self._status = TaskStatus[status]

    @property
    def status_icon(self):
        status = '□'
        if (self._status == TaskStatus.IN_PROGRESS):
            status = '◩'
        if (self._status == TaskStatus.COMPLETED):
            status = '■'
        return status

    def __str__(self):
        status = self.status_icon
        return f'{status} {self._title}'


class TaskStatus(Enum):
    CREATED = 0
    IN_PROGRESS = 1
    COMPLETED = 2

    def next(self):
        if self.value == 0:
            return TaskStatus.IN_PROGRESS
        elif self.value == 1:
            return TaskStatus.COMPLETED
        else:
            return TaskStatus.CREATED
