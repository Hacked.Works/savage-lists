import uuid
from bottle import route, run, response, request, abort, get, post, redirect

from renderer import html_renderer

from models.task import Task, TaskStatus
from persistence import persistence


@get('/tasks/<id>')
def get_task(id):
    accepted_media_types = _get_accepted_media_types()

    if _is_html_ouput_requested(accepted_media_types):
        task = _get_task(id)
        if task is None:
            return abort(404, "The requested task could not be found!")

        response.content_type = 'text/html'
        return html_renderer.render_task(task)

    abort(406, "The requested media type is not supported!")


@post('/tasks/<id>')
def post_task(id):
    accepted_media_types = _get_accepted_media_types()

    # TODO Check format of post data
    method = request.forms.get('method')

    if 'DELETE' == method:
        task = _get_task(id)
        if task is None:
            return abort(404, "The requested task could not be found!")

        persistence.delete_task_recursively(task)
        return redirect("../tasks")

    new_status = request.forms.get('status')
    new_title = request.forms.get('tasktitle')

    if _is_html_ouput_requested(accepted_media_types):
        task = _get_task(id)
        if task is None:
            return abort(404, "The requested task could not be found!")

        if new_title is not None:
            try:
                task.title = new_title
            except ValueError as e:
                abort(400, str(e))
        if new_status is not None:
            try:
                task.status = new_status
            except:
                abort(400, f"The provided status '{new_status}' is not valid!")

        persistence.update_task(task)

        return redirect("../tasks")

    abort(406, "The requested media type is not supported!")


@get('/tasks/new')
def get_new_task_form():
    accepted_media_types = _get_accepted_media_types()

    if _is_html_ouput_requested(accepted_media_types):
        response.content_type = 'text/html'
        return html_renderer.render_new_task_form(None)

    abort(406, "The requested media type is not supported!")


@get('/tasks/<id>/subtasks/new')
def get_new_subtask_form(id):
    accepted_media_types = _get_accepted_media_types()

    task = _get_task(id)
    if task is None:
        return abort(404, "The requested parent task could not be found!")

    if _is_html_ouput_requested(accepted_media_types):
        response.content_type = 'text/html'

        return html_renderer.render_new_task_form(task)

    abort(406, "The requested media type is not supported!")


@post('/tasks/<id>/tasks')
def post_new_subtask(id):
    accepted_media_types = _get_accepted_media_types()

    parent_task = _get_task(id)
    if parent_task is None:
        return abort(404, "The requested parent task could not be found!")

    # TODO Check format of post data
    title = request.forms.get('tasktitle')

    try:
        task = Task(title, parent=parent_task)
    except ValueError as e:
        abort(400, str(e))
    persistence.add_task(task)

    if _is_html_ouput_requested(accepted_media_types):
        return redirect("../../tasks")

    abort(406, "The requested media type is not supported!")


@post('/tasks')
def post_new_task():
    accepted_media_types = _get_accepted_media_types()

    # TODO Check format of post data
    title = request.forms.get('tasktitle')

    try:
        task = Task(title)
    except ValueError as e:
        abort(400, str(e))
    persistence.add_task(task)

    if _is_html_ouput_requested(accepted_media_types):
        return redirect("../tasks")

    abort(406, "The requested media type is not supported!")


@get('/tasks')
def get_tasks():
    accepted_media_types = _get_accepted_media_types()

    tasks = persistence.read_all_tasks()

    if _is_html_ouput_requested(accepted_media_types):
        response.content_type = 'text/html'
        return html_renderer.render_tasklist(tasks)

    abort(406, "The requested media type is not supported!")


@get('/')
def get_index():
    return redirect("tasks")


def _get_accepted_media_types():
    accept_header = request.headers.get('Accept')
    accepted_media_types = {}
    if accept_header is not None:
        accepted_media_types = accept_header.split(',')
    return accepted_media_types


def _is_html_ouput_requested(accepted_media_types):
    return ('text/html' in accepted_media_types) or ('*/*' in accepted_media_types) or (len(accepted_media_types) == 0)


def _get_task(id: str) -> Task:
    try:
        task_id = uuid.UUID(id)
    except:
        return None

    task = persistence.read_task(task_id)
    return task


run(host='localhost', port=8080, debug=True)
