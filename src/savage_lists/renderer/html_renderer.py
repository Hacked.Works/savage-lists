import html

HEADER = '''<!DOCTYPE html>
<html>
  <head>
    <title>Savage Lists</title>
    <style>
        .message {
            background-color: #04AA6D;
            color: #FFFFFF;

            border-radius: 5px;

            font-size: 17px;
            font-family: 'Source Sans Pro', sans-serif;
            padding: 6px 18px;

            width: 50%;
            margin: auto;
        }
    </style>
  </head>
  <body>
'''

FOOTER = '''
  </body>
</html>
'''


def render_message(message: str) -> str:
    return f'{HEADER}<div class="message">{message}</span></div>{FOOTER}'


def render_task_overview() -> str:
    return f'''{HEADER}
    <h1>Task</h1>
    <p>You can...</p>
    <ul>
      <li><a href="tasks/new">create a new task</a></li>
    </ul>
    {FOOTER}'''


def render_task(task) -> str:
    return f'''{HEADER}
    <h1>Task</h1>
    <form method="post" action="{task.task_id}">
    <label for="tasktitle">Title </label><input type="text" name="tasktitle" value="{_html_escape(task.title)}"><br>
    <input type="hidden" name="method" value="POST">
    <input type="hidden" name="status" value="{task.status.name}">
    <input type="submit" value="Update">
    </form>
    <form method="post" action="{task.task_id}">
    <input type="hidden" name="method" value="DELETE">
    <input type="submit" value="Delete">
    </form>
    <a href="../tasks/{task.task_id}/subtasks/new">create a new sub-task</a><br>
    <a href="../tasks">list tasks</a><br>
    {FOOTER}'''


def render_new_task_form(task) -> str:
    title = 'New Task'
    if task is not None:
        title = f'New Subtask to "{task.title}"'
    return f'''{HEADER}
    <h1>{title}</h1>
    <form method="post" action="../tasks">
    <input type="hidden" name="method" value="PUT">
    <label for="tasktitle">Title </label><input type="text" name="tasktitle">
    <input type="submit" value="Create">
    </form>
    {FOOTER}'''


def _html_escape(task_text):
    return html.escape(task_text).encode(
        'ascii', 'xmlcharrefreplace').decode()


def render_task_item(task):
    task_id = str(task.task_id)
    task_text = _html_escape(task.title)
    task_status = _html_escape(task.status_icon)
    task_next_status = task.status.next().name
    task_status_form = f'''
    <form method="post" action="tasks/{task_id}">
        <input type="hidden" name="status" value="{task_next_status}">
        <input type="submit" value="{task_status}"> 
        <a href="tasks/{task_id}">{task_text}</a>
    </form>
    '''

    subtasks = ''
    if len(task.children) > 0:
        subtasks = '<ul>'
        for child_task in task.children:
            subtasks = subtasks + render_task_item(child_task)
        subtasks = subtasks + '</ul>'

    item = f'''<li>{task_status_form}{subtasks}</li>'''
    return item


def render_tasklist(tasks: list) -> str:
    list_elements = []

    for task in tasks:
        if task.parent is not None:
            continue

        task_item = render_task_item(task)
        list_elements.append(task_item)

    list_items = '\n'.join(list_elements)
    return f'''{HEADER}
    <h1>Tasks</h1>
    <ul>
    {list_items}
    </ul>
    <a href="tasks">refresh task list</a><br>
    <a href="tasks/new">create a new task</a>
    {FOOTER}'''
