# Design documentation

## User stories

### A simple list of tasks ✓
As a user I want to create tasks that have a simple verb-noun like text as 
description (e.g. 'Buy groceries'). These tasks then need to be shown as a list 
and I want to be able to mark tasks on the list as completed.

https://codeberg.org/Hacked.Works/savage-lists/issues/1

### Visualize task status with checkboxes ✓
In addtion to user story 1. I want to see the status of a task as a checkbox 
that is an empty square when the task has not been worked on or a filled square 
if the task is completed. I also want to see whether a task is currently in 
progress. This requires a new third status and ideally I can change status by
clicking on the square.

https://codeberg.org/Hacked.Works/savage-lists/issues/2

### Edit and delete tasks ✓
I also want to be able to edit the description (and possibly other properties)
of a task. And of course I also want to be able to delete tasks (if they have
been complete or become obsolete).

See https://codeberg.org/Hacked.Works/savage-lists/issues/3

### Arrange tasks 
As a user I want to arrange tasks in a hierarchical order so that a task can
have multiple subtasks. But any task can have only one parent task. Also I
want to change the order of tasks. 

https://codeberg.org/Hacked.Works/savage-lists/issues/4

https://codeberg.org/Hacked.Works/savage-lists/issues/5

### Promote tasks to projects
As a user I want to promote a top level task (no parent taks) to be a project.
I also want to be able to demote a project back to a top level task. Projects
should a have their own list view.

https://codeberg.org/Hacked.Works/savage-lists/issues/6

### Create views 
I want to create views where I include and arrange tasks and subtasks to create
lists of tasks. A task can be included in multiple views. All aspects of the
tasks are accessible in these views.

https://codeberg.org/Hacked.Works/savage-lists/issues/7

## Some more ideas
- tasks can be proxies for other todo/task like artefacts. For example a task could
be tied to a GitHub/GitLab/Codeberg issue and reflect the current state of that issue.
This should help to reduce redundant data.

- tasks could have: tags, a priority, due date, estimated duration, a color, and be 
assigned to a cathegory